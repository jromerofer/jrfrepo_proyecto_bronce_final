//ALTA DE USUARIOS

const crypt = require ('../crypt');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujrf6ed/collections/";
const mLabAPIKey = "apiKey=cKW1_6BGSGLHFUmz0If5HlyuBKRT6F32";
const requestJson = require('request-json');


//GET USER V2 BBDD
function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Buscando Clientes en BD");
  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body) {
      var response =!err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  );
}

//GET USERS V2 BY ID BBDD
function getUsersByV2(req, res) {

  var id = req.params.id;
  var query = 'q={"user_id":'+ id + '}'; //comilla simple es un string literal. Estamos componiendo la query que le vamos a pasar
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("GET /apitechu/v2/users/:id");
  console.log("Buscando Cliente en BD");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {

      console.log("LENGTH: " + body.length);
      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);

        } else {
          if (body.length > 0) {
            var response = body[0];
            } else {
                var response = {
                "msg" : "Usuario no encontrado"
                }
                res.status(404);
              }
        }

      res.send(response);

    }
  );
}

//POST CREATE USER V2 BBDD
function createUsersV2 (req, res) {

  console.log("user_id es: " + req.body.user_id);
  console.log("first_name es: " + req.body.first_name);
  console.log("last_name es: " + req.body.last_name);
  console.log("email es: " + req.body.email);
  console.log("password sin encriptar: " + req.body.password);

function randomFromInterval(min,max) {
  return Math.floor(Math.random() * (max-min+1)+min);
}

var userid = randomFromInterval(1000, 9999);

  var newUser = {
//  "user_id" : req.body.user_id,
    "user_id" : userid,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  };

    console.log("Encriptado de password");
    console.log (newUser)

  var httpClient = requestJson.createClient(baseMlabURL);

    console.log("Escribiendo usuario en BBDD");

  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body) {

      if (err) {

        console.log("Error en el proceso de alta de usuario")
        res.send({"msg" : "Error creando usuario"})

      } else {
        console.log("Usuario creado con éxito")
        res.send({"msg" : "Usuario creado correctamente"})
        }
    }
  );
}

//EXPORTS DE LLAMADAS

module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByV2 = getUsersByV2;
module.exports.createUsersV2 = createUsersV2;
