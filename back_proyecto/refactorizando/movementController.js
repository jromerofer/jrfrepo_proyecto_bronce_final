//GESTION DE MOVIMIENTOS//

/***********************************************************************************/
//CONSTANTES DE TRABAJO
  const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujrf6ed/collections/";
  const mLabAPIKey = "apiKey=cKW1_6BGSGLHFUmz0If5HlyuBKRT6F32";
  const requestJson = require('request-json');

/**
//GET ACCOUNT BALANCE
function getAccBalance (req, res) {
  console.log("GET /apitechu/v2/accounts");
  var query = 'q={"account_id":' + req.body.account_id + '}';
  console.log("query es " + query);
  console.log("Buscando balance de cuenta");
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        console.log("Error obteniendo Balance");
        res.send({"msg" : "Error obteniendo balance. No es posible realizar operación. Cosulte con su gestor comercial."});
      } else {
        var preBalance = (body[0].balance);
        console.log("Balance antes de operción: " + preBalance);
      }
    }
  );
}
**/

//GET MOVEMENT
function getMovements (req, res) {
  console.log("GET /apitechu/v2/movements");
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Obteniendo movimientos de cuenta");
  httpClient.get("movement?" + mLabAPIKey,
    function(err, resMLab, body) {
      var response =!err ? body : {
        "msg" : "Error obteniendo movimientos"
      }
      res.send(response);
    }
  );
}

//POST CREATE MOVEMENT
function createMovements (req, res) {

  // Variables de operativa
  var entryId = randomFromInterval(1000, 9999);
  var absAmount = absFrontAmount(req.body.amount);
  //  var operFinBalance = (req.body.amount + req.body.balance)
  //Variables de operación
  var isIngreso = "Ingreso";
  var isReintegro = "Reintegro";
  //Variables de fecha
  var fecha = new Date();
  var aaaa = fecha.getFullYear();
  var mes1 = (fecha.getMonth()+1); //Enero es el 0
  var mes2 = parseInt(mes1,10);
      if (mes2 < 10) {
      var cerito = "0";
      var mes = cerito.concat(mes1);
      } else {
        var mes = mes1;
      }
  var dia = fecha.getDate();
  var hora = fecha.getHours();
  var min = fecha.getMinutes();
  var entryDate = (aaaa + "/" + mes + "/" + dia + " " + hora + ":" + min);

  console.log("Empiezo operativa de creacion de movimiento *************************************************");
  console.log("Revisando condiciones del apunte: ");
  console.log("Fecha de entrada para realizar movimiento: " + entryDate);
  console.log("entry_id: " + "SERA GENERADO BY f(x)RANDOM");
  console.log("account_id: " + req.body.account_id);
  console.log("user_id: " + req.body.user_id);
  console.log("amount: " + req.body.amount);
  console.log("IBAN_IN: " + req.body.IBAN_IN);
  console.log("sumary: " + req.body.sumary);
  console.log("type_entry: " + req.body.type_entry);
  console.log("date_entry: " + req.body.date_entry);
  console.log("balance POSTMAN: " + req.body.balance);
  console.log("FIN ENTRADA DE DATOS *************************************************************************");
  //  console.log("Balance resultado para la operación: "+ operFinBalance);

  //OPERATIVA*******************************************************************
  if ((req.body.amount) === 0) {
    var response = {
      "mensaje" : "Tipo de operación o importe incorrecto. Debe indicar importe positivo distinto de '0' " +
      "y tipo de operación (Ingreso o Reintegro)."
    }
      console.log("Cantidad (req.body.amount) incorrecta para el apunte: " + req.body.amount);
      res.send(response);
  } else {
      console.log("Buscando balance de cuenta");
      //console.log("URL=" + baseMlabURL + "movement?" + mLabAPIKey);
      var query = ('q={"account_id":' + req.body.account_id + '}');
      var httpClient = requestJson.createClient(baseMlabURL);
      //var preBalance = 0 ;
      //console.log("PREBALANCE: " + preBalance)
      httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            console.log("Error obteniendo Balance");
            res.send({"msg" : "Error obteniendo balance. No es posible realizar operación. Cosulte con su gestor comercial."});
          } else {
      //    console.log(body[0]);
            var preBalance = (body[0].balance);
            console.log("Balance en cuenta antes de movimiento: " + preBalance);

      if ((absAmount + preBalance) <= 0) {
        var response = {
          "mensaje1" : "Balance inicial o resultante de la operacion negativo.",
          "mensaje2" : "Sólo permitidas operaciones con balance inicial o resultante positivo.",
          "mensaje3" : "Consulte con su Gestor para linea de crédito"
        }
        console.log("Cantidad incorrecta para el apunte: " + req.body.amount);
        res.send(response);

        } else {
            if (req.body.type_entry === isIngreso) {
                var balance = (preBalance + req.body.amount);
                console.log("Operación tipo -->INGRESO : " + req.body.type_entry + " en curso. Cantidad del movimiento: " + req.body.amount + " Nuevo balance: " + balance);
            } else {
                if (req.body.type_entry === isReintegro) {
                  var balance = (preBalance + req.body.amount);
                  console.log("Operación tipo -->REINTEGRO : " + req.body.type_entry + " en curso. Cantidad del movimiento: " + req.body.amount + " Nuevo balance: " + balance);
                }
              }

            var newMovement = {
              "entry_id" : entryId,
              "account_id" : req.body.account_id,
              "user_id" : req.body.user_id,
              "amount" : absAmount,
              "IBAN_IN" : req.body.IBAN_IN,
              "sumary" : req.body.sumary,
              "type_entry" : req.body.type_entry,
              "entry_date" : entryDate,
              "balance" : balance
            };

            console.log("INI DATOS DE ENTRADA PARA MOVIVIENTO");
            console.log(entryId);
            console.log(req.body.account_id);
            console.log(req.body.user_id);
            console.log(absAmount);
            console.log(req.body.IBAN_IN);
            console.log(req.body.sumary);
            console.log(req.body.type_entry);
            console.log(entryDate);
            console.log(balance);
            console.log("FIN DATOS DE ENTRADA PARA MOVIVIENTO");

            console.log("URL=" + baseMlabURL + "movement?" + mLabAPIKey)
            var httpClient = requestJson.createClient(baseMlabURL);
            httpClient.post("movement?" + mLabAPIKey, newMovement,
              function(err, resMLab, body) {
                if (err) {
                  console.log("Error en POST de movimiento");
                  res.send({"msg" : "Error realizando movimiento"});
                } else {
                  console.log("Movimiento de " + req.body.type_entry + " realizado. Actualizando balance en cuenta.");

                  query = 'q={"account_id" : ' + req.body.account_id + '}';
                  var putBody = '{"$set":{"balance":'+ balance + '}}';
                  console.log("URL=" + baseMlabURL + "movement?" + mLabAPIKey);
                  var httpClient = requestJson.createClient(baseMlabURL);
                  httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                    function(errPUT, resMLabPUT, putBody) {
                      if (errPUT) {
                        console.log("Error en actualización de balance")
                        res.send({"msg" : "Error actualizando balance en cuenta"})
                      } else {
                          var response = {
                            "msg1" : "Movimiento de " + req.body.type_entry + " realizado.",
                            "msg2" : "Nuevo balance: "  + balance,
                            "msg3" : "Fecha Movimiento: " + entryDate
                          }
                          console.log("Actualizado balance de cuenta");
                          res.send(response);
                        }
                    }
                  );
                }
              }
            );
          }
        }
      }
    );
    }

}

//MATH FUNTIONS
function randomFromInterval(min,max) {
  return Math.floor(Math.random() * (max-min+1)+min);
}
function absFrontAmount(num) {
  return Math.abs(num);
}
//EXPORTS DE LAS LLAMADAS
//module.exports.getAccBalance = getAccBalance;
module.exports.getMovements = getMovements;
module.exports.createMovements = createMovements;
//NOTAS
  /**var newMovement = {
    "entry_id": entry_id,
    "account_id": req.body.account_id,
    "user_id": req.body.user_id,
    "amount": req.body.amount,
    "IBAN_IN": req.body.IBAN_IN,
    "sumary": req.body.sumary,
    "type_entry": req.body.type_entry,
    "entry_date": req.body.date_entry,
    //"balance": req.body.balance
    "balance": balance
  };**/
  /**
  var newMovement = {
    "entry_id": 11,
    "account_id": 1,
    "user_id": 1,
    "amount": 268,
    "IBAN_IN": "ME05 8850 4946 5495 1435 72",
    "sumary": "ligula nec sem",
    "type_entry": "Ingreso",
    "date_entry": "08/10/2017",
    "balance": 368
  };
  **/
