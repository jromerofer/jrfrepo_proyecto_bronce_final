//GESTION DE MOVIMIENTOS//
//INGRESO Y REINTEGRO//
//BALANCE//


//INGRESO Y REINTEGRO CON ACTUALIZACIÓN DE BALANCE//
/** ******************************************************************************* **/

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujrf6ed/collections/";
const mLabAPIKey = "apiKey=cKW1_6BGSGLHFUmz0If5HlyuBKRT6F32";
const requestJson = require('request-json');

//GET ACCOUNT BY USER
function getAccBalance (req, res) {
  console.log("GET /apitechu/v2/accounts");
  var query = 'q={"user_id":' + req.body.user_id + '}';
  console.log("query es " + query);

  var httpClient = requestJson.getAccBalance(baseMLabURL);

  console.log("Buscando cuentas");
  httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
      var response =!err ? body : {
        "msg" : "Error obteniendo cuentas"
      }
      res.send(response);
    }
  );
}

//GET MOVEMENT
function getMovements (req, res) {
  console.log("GET /apitechu/v2/movements");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Obteniendo movimientos de cuenta");
  httpClient.get("movement?" + mLabAPIKey,
    function(err, resMLab, body) {
      var response =!err ? body : {
        "msg" : "Error obteniendo movimientos"
      }
      res.send(response);
    }
  );
}

//POST CREATE MOVEMENT
function createMovements (req, res) {

  //console.log("entry_id: " + req.body.entry_id);
  console.log("entry_id: " + "GENERADO BY f(x)RANDOM");
  console.log("account_id: " + req.body.account_id);
  console.log("user_id: " + req.body.user_id);
  console.log("amount: " + req.body.amount);
  console.log("IBAN_IN: " + req.body.IBAN_IN);
  console.log("sumary: " + req.body.sumary);
  console.log("type_entry: " + req.body.type_entry);
  console.log("date_entry: " + req.body.date_entry);
  console.log("balance: " + req.body.balance);


  // Las variables de account_id, user_id, entry_date y type_entry, deben llegar de la pagina en la que estoy...

  var entry_id = randomFromInterval(1000, 9999);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Realizando apunte");
  console.log("Tipo de apunte: " + req.body.type_entry);

  //Tratamiento del balance
  var isIngreso = "Ingreso";
  var isReintegro = "Reintegro";

  if ((req.body.amount) === 0) {

    var response = {
      "mensaje" : "Tipo de operación o importe incorrecto. Debe indicar importe distinto de '0' y tipo " +
      "de operación (Ingreso o Reintegro)."
    }
      console.log("Cantidad incorrecta para el apunte: " + req.body.amount);
      res.send(response);
  } else {
      if (req.body.type_entry === isIngreso) {
          var balance = (req.body.balance + req.body.amount);
          console.log("Operación tipo -->INGRESO : " + req.body.type_entry + " en curso. Cantidad del movimiento: " + req.body.amount + " Nuevo balance: " + balance);
          var response = {
            "mensaje" : "Operación de Ingreso realizada"
          }
      } else {
          if (req.body.type_entry === isReintegro) {
            //var absamount = absAmount(req.body.amount);
            var absamount = req.body.amount;
            var balance = (req.body.balance - absamount);
            console.log (absamount);
            console.log("Operación tipo -->REINTEGRO : " + req.body.type_entry + " en curso. Cantidad del movimiento: " + req.body.amount + " Nuevo balance: " + balance);
            var response = {
                "mensaje" : "Operación de Reintegro realizada"
              }
            var newMovement = {
                "entry_id": entry_id,
                "account_id": req.body.account_id,
                "user_id": req.body.user_id,
                //"amount": req.body.amount,
                "amount": absamount,
                "IBAN_IN": req.body.IBAN_IN,
                "sumary": req.body.sumary,
                "type_entry": req.body.type_entry,
                "entry_date": req.body.date_entry,
                //"balance": req.body.balance
                "balance": balance
              };
            console.log(balance);

            httpClient.post("movement?" + mLabAPIKey, newMovement,
                function(err, resMLab, body) {
                  var response =!err ? body : {
                    "msg" : "Error en creacion de Apunte"
                    }
                    res.send(response);
                  }
              );

          } else {
              var response = {
                  "mensaje" : "Tipo de operación incorrecto. Debe indicar Ingreso o Reintegro."
                }
              res.send(response);
              console.log("Tipo de operación incorrecto. Distinto de Ingreso o Reintegro.");
            }
        }
    }
}

//MATH FUNTIONS
function randomFromInterval(min,max) {
  return Math.floor(Math.random() * (max-min+1)+min);
}
function absAmount(num) {
  return Math.abs(num);
}


//EXPORTS DE LAS LLAMADAS
module.exports.getAccBalance = getAccBalance;
module.exports.getMovements = getMovements;
module.exports.createMovements = createMovements;


  /**var newMovement = {
    "entry_id": entry_id,
    "account_id": req.body.account_id,
    "user_id": req.body.user_id,
    "amount": req.body.amount,
    "IBAN_IN": req.body.IBAN_IN,
    "sumary": req.body.sumary,
    "type_entry": req.body.type_entry,
    "entry_date": req.body.date_entry,
    //"balance": req.body.balance
    "balance": balance
  };**/

  /**
  var newMovement = {
    "entry_id": 11,
    "account_id": 1,
    "user_id": 1,
    "amount": 268,
    "IBAN_IN": "ME05 8850 4946 5495 1435 72",
    "sumary": "ligula nec sem",
    "type_entry": "Ingreso",
    "date_entry": "08/10/2017",
    "balance": 368
  };
  **/
