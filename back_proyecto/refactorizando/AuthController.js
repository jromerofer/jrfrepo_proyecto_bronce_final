//LOGIN DE USUARIOS

const crypt = require ('../crypt');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujrf6ed/collections/";
const mLabAPIkey = "apiKey=cKW1_6BGSGLHFUmz0If5HlyuBKRT6F32";
const requestJson = require('request-json');


//LOGIN

function login(req, res) {
  console.log("POST /apitechu/v2/users/login");

  //var email = req.body.email;
  //var email = "jrf@email.com";
  //var password = req.body.password;
  //var password = "1234";
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("req.body.email: "+ req.body.email);
  console.log("HttpClient: "+ httpClient);
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email" : "' + email + '"}';
  console.log("QUERY : " + query);
  console.log("req.body.password");
  console.log(password);
  console.log(email);

  console.log("Comienzo proceso de login *************************************");
  //httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?" + query + "&" + mLabAPIkey,

    function(err, resMLab, body) {
      var isPasswordCorrect = crypt.checkPassword(password, body[0].password);
      console.log("Password correcta: " + isPasswordCorrect);

      if (body.length == 0 || !isPasswordCorrect) {
        var response = {
          "mensaje" : "Login incorrecto",
          "mensaje2" : "email y/o password no encontrados"
        }
        res.send(response);
      } else {
          console.log("Encontrado usuario con email y password correcto. Realizando login");
          query = 'q={"user_id" : ' + body[0].user_id +'}';
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + mLabAPIkey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              console.log("PUT realizado. LOGIN almacenado en BBDD");
              var response = {
                "mensaje" : "Usuario logado con éxito",
                "idUsuario" : body[0].user_id
              }
              res.send(response);
            }
          )
        }
    }
  );
}

//LOGOUT
function logout(req, res) {
  console.log("POST /apitechu/v2/logout/:id");
  var httpClient = requestJson.createClient(baseMLabURL);
  var query = 'q={"user_id": ' + req.params.id + '}';
  console.log("Comienzo proceso de logout *************************************");

  httpClient.get("user?" + query + "&" + mLabAPIkey,
      function(err, resMLab, body) {
      if (body.length == 0) {
        var response = {
          "mensaje" : "Logout incorrecto, usuario no encontrado"
        }
        res.send(response);
      } else {
          console.log("Encontrado usuario con email y password correcto. Realizando logout");
          query = 'q={"user_id" : ' + body[0].user_id +'}';
          var putBody = '{"$unset":{"logged":""}}';
          httpClient.put("user?" + query + "&" + mLabAPIkey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              console.log("PUT realizado. LOGIN eliminado de BBDD");
              var response = {
                "msg" : "Usuario deslogado",
                "user_id" : body[0].user_id
              }
              res.send(response);
            }
          );
        }
    }
   );
}

module.exports.login = login;
module.exports.logout = logout;
