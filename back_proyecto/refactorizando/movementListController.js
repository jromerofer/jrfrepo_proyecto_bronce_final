//GESTION DE MOVIMIENTOS//

/***********************************************************************************/
//CONSTANTES DE TRABAJO
  const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujrf6ed/collections/";
  const mLabAPIKey = "apiKey=cKW1_6BGSGLHFUmz0If5HlyuBKRT6F32";
  const requestJson = require('request-json');

//GET BALANCE ACCOUNT BY USER
function getAccBalance (req, res) {
  console.log("GET /apitechu/v2/accounts");
  var query = 'q={"user_id":' + req.body.user_id + '}';
  console.log("query es " + query);

  var httpClient = requestJson.getAccBalance(baseMLabURL);

  console.log("Buscando cuentas");
  httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
      var response =!err ? body : {
        "msg" : "Error obteniendo cuentas"
      }
      res.send(response);
    }
  );
}

//GET MOVEMENT LIST
function getMovementsList (req, res) {
  console.log("GET /apitechu/v2/accounts/movements");
  var httpClient = requestJson.createClient(baseMlabURL);
  var query = 'q={"acount_id":' + req.body.account_id + '}';
  console.log("query es " + query);
  console.log("Obteniendo movimientos de cuenta");
  httpClient.get("movement?" + mLabAPIKey,
    function(err, resMLab, body) {
      var response =!err ? body : {
        "msg" : "Error obteniendo movimientos"
      }
      res.send(response);
    }
  );
}

//EXPORTS DE LAS LLAMADAS
module.exports.getAccBalance = getAccBalance;
module.exports.getMovementsList = getMovementsList;
