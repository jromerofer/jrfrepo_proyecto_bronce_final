//GESTION DE CUENTAS//
/***********************************************************************************/

/** account
accountid
IBAN
balance
alias
date
userid
**/

//CONSTANTES DE TRABAJO
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujrf6ed/collections/";
const mLabAPIKey = "apiKey=cKW1_6BGSGLHFUmz0If5HlyuBKRT6F32";
const requestJson = require('request-json');

//Variables de fecha
  var fecha = new Date();
  var aaaa = fecha.getFullYear();
  var mes1 = (fecha.getMonth()+1); //Enero es el 0
  var mes2 = parseInt(mes1,10);
      if (mes2 < 10) {
      var cerito = "0";
      var mes = cerito.concat(mes1);
      } else {
        var mes = mes1;
      }
  var dia = fecha.getDate();
  var hora1 = fecha.getHours();
  var hora2 = parseInt(hora1,10);
      if (hora2 < 10) {
      var cerito = "0";
      var hora = cerito.concat(hora1);
      } else {
        var hora = hora1;
      }
  var min1 = fecha.getMinutes();
  var min2 = parseInt(min1,10);
      if (min2 < 10) {
      var cerito = "0";
      var min = cerito.concat(min1);
      } else {
        var min = min1;
      }

  var dates = (aaaa + "/" + mes + "/" + dia + " " + hora + ":" + min);

//GET ACCOUNT"S" BY USER
function getAccountsV2(req, res) {
  console.log("GET /apitechu/v2/users/:id/accounts");
  console.log("Comienzo proceso de buscar cuentas de usuario ******************");
  console.log("REQ.params.id: " + req.params.id);

  var id = req.params.id;
  var query = 'q={"userid" : ' + id + '}';
  console.log("QUERY: " + query);

  var httpClient = requestJson.createClient(baseMLabURL);

  console.log("Validación usuario logado por ID .............................");
  console.log(baseMLabURL + "user?" + query +"&"+ mLabAPIKey);

  httpClient.get("user?"+ query +"&"+ mLabAPIKey,
    function(err,resMLab, body) {
      if(err){
        var response={
          "msg" : "Error obteniendo el usuario"
        };
        res.status(500);
        res.send(response);
      } else if (body.length > 0 && body[0].logged !== true) {
        var response={
          "msg" : "Usuario no logado"
        };
        res.status(400);
        res.send(response);
        console.log("USUARIO NOOOO LOGADO");

      } else if(body.length > 0 && body[0].logged === true) {
        var httpClient = requestJson.createClient(baseMLabURL);
        var query = 'q={"userid" : ' + id + '}';
        console.log("BUSCANDO CUENTAS DE USUARIO:");
        console.log("QUERY: " + query);
        console.log(baseMLabURL + "account?" + query +"&"+ mLabAPIKey);
        httpClient.get("account?"+ query +"&"+ mLabAPIKey,
          function(err,resMLab, body) {
            if(err){
              console.log("ERROR BUSCANDO CUENTAS DE USUARIO: "+ err);
              var response = {
                "msg" : "Error obteniendo cuentas"
              };
            } else {
              if (body.length > 0) {
                var response = body;
                console.log("BODY DE CUENTAS DE USUARIO: " + body);
                console.log("CUENTAS DE USUARIO RECUPERADAS OK ****************");
              } else {
                var response = {
                  "msg" : "Cuentas de usuario no encontradas. Contacte con su gestor o cree una"
                };
                console.log("CUENTAS DE USUARIO NO ENCONTRADAS ****************");
              }
            }
            res.send(response);
          }
        );
      }
    }
  );
}

//GET ACCOUNT BY USER
function getAccountsByIdV2(req, res) {
  console.log("Comienzo proceso de buscar CUENTA de usuario por ID de Cuenta **");
  console.log("GET /apitechu/v2/users/:id/accounts/:accountid");

  var id = req.params.id;
  var accountid = parseInt(req.params.accountid);
  var query = 'q={"$and" : [{"userid" : ' + id + '},'+'{"accountid" : '+ accountid + '}]}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Buscando cuenta de usuario por ID ............................");
  console.log(baseMLabURL + "account?" + query +"&"+ mLabAPIKey);
  httpClient.get("account?" + query +"&"+ mLabAPIKey,
    function(err,resMLab, body) {
      if(err){
        var response={
          "msg" : "Error obteniendo cuentas"
        };
        res.status(500);
      } else {
        if(body.length > 0) {
          response = body;
        } else {
          var response={
            "msg" : "Cuenta no encontrada"
          };
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

//POST CREATE ACCOUNT
function createAccountV2(req, res) {
  console.log("Comienzo proceso de alta de cuenta *****************************");
  console.log("POST /apitechu/v2/users/:id/accounts");
  console.log("accountid: " + "GENERADO BY f(x)RANDOM");
  console.log("balance inicial: " + req.body.amount);
  console.log("alias: " + req.body.alias);

  var accountIdRand = randomFromInterval(1000, 9999);
  var ibanRand = randomFromInterval(1000, 9999) + " " +randomFromInterval(1000, 9999) + " " +randomFromInterval(1000, 9999) + " " +randomFromInterval(1000, 9999) + " " +
  randomFromInterval(1000, 9999);

  //var balance_ini = 0;
  var balance_ini = parseInt(req.body.balance);
  var id = req.params.id;

  var newAccount = {
    "userid" : parseInt(req.params.id),
    "accountid" : accountIdRand,
    "IBAN" : ibanRand,
    "alias" : req.body.alias,
    "balance" : balance_ini,
    "date": dates
  };

  console.log(newAccount);

  var query = 'q={"userid" : ' + id + '}';
  var httpClient = requestJson.createClient(baseMLabURL);

  console.log("Validación usuario logado por ID .............................");
  console.log(baseMLabURL+"user?" + query +"&"+ mLabAPIKey);

  httpClient.get("user?"+ query +"&"+ mLabAPIKey,
    function(err,resMLab, body) {
      if(err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        };
        res.send(response);
      } else if (body.length > 0 && body[0].logged !== true) {
        var response = {
          "msg" : "Usuario no logado"
        };
        res.status(400);
        res.send(response);
      } else if (body.length > 0 && body[0].logged === true) {
          var httpClient = requestJson.createClient(baseMLabURL);
          var query = 'q={"userid" : ' + id + '}';
          console.log("Creando Cuenta ...............................................");
          console.log(baseMLabURL + "account?" + query +"&"+ mLabAPIKey);
          httpClient.post("account?" + query +"&"+ mLabAPIKey, newAccount,
            function(err,resMLab, body) {
              if (err) {
                console.log("Error en el proceso de alta de cuenta");
                res.send(
                  {"msg" : "Error creando cuenta"}
                )
              } else {
                console.log("Realizada alta de cuenta ***************************************");
                res.send(
                  {"msg" : "Realizada alta de cuenta"}
                )
              }
            }
          );
        }
    }
  );
}


//GET MOVEMENT
function getMovementsV2(req, res) {
  console.log("Comienzo proceso de buscar movimientos de la CUENTA de usuario ************");
  console.log("GET /apitechu/v2/users/:id/accounts/:accountid/movements");

  var id = req.params.id;
  var accountid = req.params.accountid;
  var query = 'q={"userid" : ' + id + '}';

  console.log("req.params.id (nº usuario): " + req.params.id);
  console.log("req.params.accountid (nº usuario): " + req.params.accountid);
  console.log("QUERY: " + query);

  var httpClient = requestJson.createClient(baseMLabURL);

  console.log("Validación usuario logado por ID .............................");
  console.log(baseMLabURL + "user?" + query +"&"+ mLabAPIKey);

  httpClient.get("user?" + query +"&"+ mLabAPIKey,
    function(err,resMLab, body) {
      if(err) {
        var response={
          "msg" : "Error obteniendo el usuario"
        };
        res.status(500);
        res.send(response);
        console.log("USUARIO NOOOO ENCONTRADO POR ID");
      } else if (body.length > 0 && body[0].logged !== true) {
        var response={
          "msg" : "Usuario no logado"
        };
        res.status(401);
        res.send(response);
        console.log("USUARIO NOOOO LOGADO");

      } else if(body.length > 0 && body[0].logged === true) {
          console.log("USUARIO ENCONTRADO .......................................");

          var httpClient = requestJson.createClient(baseMLabURL);
          var query = 'q={"$and" : [{"userid" : ' + id + '},'+'{"accountid" : '+ accountid + '}]}';
          console.log("REQUEST Buscando movimientos para la cuenta ................................");
          console.log(baseMLabURL+"movement?" + query +"&"+ mLabAPIKey);
          httpClient.get("movement?"+ query +"&"+ mLabAPIKey,
            function(err,resMLab, body) {
              if(err) {
                var response={
                  "msg" : "Error obteniendo movimientos"
                };
                res.status(500);
                console.log("ERROR DE ACCESO AL SERVIDOR.");
              } else {
                if(body.length > 0) {
                  response = body;
                  console.log("MOVIMIENTOS ENCONTRADOS EN CUENTA");
                } else {
                  var response={
                    "msg" : "Movimientos no encontrados"
                  };
                  res.status(404);
                  console.log("NO ENCONTRADOS MOVIMIENTOS PARA MOSTRAR");
                }
              }
              res.send(response);
            }
          );
      }
      //res.send(response);
    }
  );
}

function createMovementV2(req, res) {
  console.log("POST /apitechu/v2/users/:id/accounts/:accountid/movements");
  console.log("Empiezo operativa de alta de movimiento *************************************************");
  console.log("Revisando condiciones del apunte: ");


  // Variables de operativa
    var entryId = randomFromInterval(1000, 9999);

    console.log("entryId: " + entryId);

    //var absAmount = absFrontAmount(req.body.amount);
    //var operFinBalance = (req.body.amount + req.body.balance)
    //Variables de operación
    var isIngreso = "Ingreso";
    var isReintegro = "Reintegro";

    //Variables de fecha
    var fecha = new Date();
    var aaaa = fecha.getFullYear();
    var mes1 = (fecha.getMonth()+1); //Enero es el 0
    var mes2 = parseInt(mes1,10);
        if (mes2 < 10) {
        var cerito = "0";
        var mes = cerito.concat(mes1);
        } else {
          var mes = mes1;
        }
    var dia = fecha.getDate();
    var hora1 = fecha.getHours();
    var hora2 = parseInt(hora1,10);
        if (hora2 < 10) {
        var cerito = "0";
        var hora = cerito.concat(hora1);
        } else {
          var hora = hora1;
        }
    var min1 = fecha.getMinutes();
    var min2 = parseInt(min1,10);
        if (min2 < 10) {
        var cerito = "0";
        var min = cerito.concat(min1);
        } else {
          var min = min1;
        }

  var dates = (aaaa + "/" + mes + "/" + dia + " " + hora + ":" + min);

  console.log("Fecha de entrada para realizar movimiento: " + dates);

  var query = 'q={"$and" : [{"userid" : ' + req.params.id + '},'+'{"accountid" : ' + req.params.accountid + '}]}';

  var newMovement =
  {
    "userid" : parseInt(req.params.id),
    "accountid" : parseInt(req.params.accountid),
    "entryid" : entryId,
    "type" : req.body.type,
    "sumary" : req.body.sumary,
    "date" : dates,
    "amount" : req.body.amount,
    "balance" : 0
  };

  console.log(newMovement);

  var httpClient = requestJson.createClient(baseMLabURL);
  var newBalance = 0; //inicializamos el balance

  console.log("Empiezo operativa de balance .................................");
  console.log("GET Balance=" + baseMLabURL + "account?" + query +"&"+ mLabAPIKey);

  httpClient.get("account?" + query +"&"+ mLabAPIKey,
    function(err, resMLab, body) {
      if(err) {
        var response = {
          "msg" : "Error obteniendo cuenta para balance"
        };
        res.status(500);
        res.send(response);
      } else {
        if(body.length > 0) {
          newBalance = parseFloat(body[0].balance);
          if(newMovement.type === isIngreso) {
            newBalance = newBalance + parseFloat(newMovement.amount);
          } else if (newMovement.type === isReintegro) {
            newBalance = newBalance - parseFloat(newMovement.amount);
          }

          console.log("Nuevo balance resultado para la operación: " + newBalance);

          if(newBalance < 0) {
            var response = {
              "msg" : "Saldo negativo no permitido. Consulte crédito con el gestor."
            };
            console.log("ERR: " + response);
            res.send(response);

          } else {
            newMovement.balance = newBalance.toFixed(2);

            console.log("operFinBalance: " + newMovement.balance);

            httpClient.post("movement?" + mLabAPIKey, newMovement,
              function(err, resMLab, body) {
                var putBody = '{"$set":{"balance":' + newMovement.balance + '}}';

                console.log("POST Movimiento: " + baseMLabURL+ "movement?" + query +"&"+ mLabAPIKey);
                console.log("PUT Balance: " +baseMLabURL+ "account?" + query +"&"+ mLabAPIKey);

                httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                  function(errPUT, resMLabPUT, bodyPUT) {
                    var response = {"msg" : "Operación realizada. Nuevo balance: " + newMovement.balance + " €"}
                    res.send(response);

                    console.log("Movimiento de " + req.body.type + " realizado. Actualizando balance en cuenta. ******************** " + newMovement.balance);
                  }
                );
              }
            );
          }
        } else {
            var response={"msg" : "Balance no actualizado. Error en cuenta."};
            res.send(response);
        };
      }
    }
  )
}


function randomFromInterval(min,max) {
  return Math.floor(Math.random() * (max-min+1)+min);
}

module.exports.getAccountsV2 = getAccountsV2;
module.exports.createAccountV2 = createAccountV2;
module.exports.getAccountsByIdV2 = getAccountsByIdV2;
module.exports.getMovementsV2 = getMovementsV2;
module.exports.createMovementV2 = createMovementV2;
