//LOGIN/OUT DE USUARIOS

const crypt = require('../crypt');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujrf6ed/collections/";
const mLabAPIkey = "apiKey=cKW1_6BGSGLHFUmz0If5HlyuBKRT6F32";
const requestJson = require('request-json');

//LOGIN
function loginV2(req, res) {

  console.log("Comienzo proceso de LOGIN ***************************************");
  console.log("POST /apitechu/v2/login");
  //var email = "jrf@email.com";
  //var password = "1234";
  var email = req.body.email;
  var password = req.body.password;

  console.log("Validación de email y pass sin dato............................");

  if (email == "" || password == "" ) {
    var response = {
      "msg" : "Email o pass no informados"
    };
    console.log("EMAIL O PASSWORD NOOO!!! INFORMADOS ............................");
    res.send(response);
  }
  console.log("EMAIL Y PASSWORD INFORMADOS  ............................");
  console.log("req.body.email: "+ req.body.email);
  console.log("req.body.password: "+ req.body.password);

  console.log("1er request proceso de login ............................");

  var query = 'q={"email": "' + email + '"}';
  console.log("query es " + query);

  //var httpClient = requestJson.createClient(baseMLabURL);
  //console.log("req.body.email: "+ req.body.email);
  //console.log("HttpClient: "+ httpClient);

  console.log("Buscando usuario para validación de PASSWORD............................");
  console.log("Validación de LENGTH sin dato..........................................");

  httpClient = requestJson.createClient(baseMLabURL);
    httpClient.get("user?" + query + "&" + mLabAPIkey,
      function(err, resMLab, body) {
        console.log("LENGTH: " + body.length);
        if (body.length == 0) {
          var response = {
            "msg" : "Login incorrecto; email y/o passsword no encontrados"
          };
          res.send(response);
        } else {
            var isPasswordcorrect = crypt.checkPassword(password, body[0].password);
            console.log("¿Password correcta (TRUE/FALSE)? " + isPasswordcorrect);
            if (!isPasswordcorrect) {
              var response = {
                "msg" : "Login incorrecto, email y/o passsword no encontrados"
              };
              console.log("USUARIO NOOOOO!!!!!  VALIDADO ........................................");
              res.send(response);
            } else {
                console.log("USUARIO VALIDADO .....................................................");
                console.log("2o request proceso de login PUT logged................................");
                query = 'q={"userid" : ' + body[0].userid +'}';
                console.log("QUERY - PUT: " + query);
                var putBody = '{"$set":{"logged":true}}';
                httpClient.put("user?" + query + "&" + mLabAPIkey, JSON.parse(putBody),
                  function(errPUT, resMLabPUT, bodyPUT) {
                    if(errPUT) {
                      var response = {
                        "msg" : "Error en el Login. No te has logado. Inténtalo de nuevo."
                      }
                      console.log("Error al realizar el PUT del logged del usuario en BBDD ...........................");
                    } else {
                        var response = {
                          "msg" : "Usuario logado con éxito.",
                          "idUsuario" : body[0].userid
                        }
                        res.send(response);
                        console.log("BODY: " + body[0].userid);
                        console.log("PUT LOGIN realizado en BBDD ******************************************");
                      }
                  }
                );
              }
          }
      }
    );
}

//LOGOUT
function logoutV2(req, res) {
  console.log("Comienzo proceso de LOGOUT *************************************");
  console.log("POST /apitechu/v2/logout/:id");

  var query = 'q={"userid": ' + req.params.id + '}';
  console.log("query es " + query);

  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + query + "&" + mLabAPIkey,
  function(err, resMLab, body) {
    if (body.length == 0) {
      var response = {"mensaje" : "Logout incorrecto, usuario no encontrado"};
      res.send(response);
    } else {
      console.log("Got a user with that id, logging out");
      query = 'q={"userid" : ' + body[0].userid +'}';
      console.log("QUERY LOGOUT: " + query);
      var putBody = '{"$unset":{"logged":""}}'
      httpClient.put("user?" + query + "&" + mLabAPIkey, JSON.parse(putBody),
        function(errPUT, resMLabPUT, bodyPUT) {
          console.log("PUT realizado. LOGGDED eliminado de BBDD");
          var response = {
            "msg" : "Usuario deslogado",
            "userid" : body[0].userid
            //"idUsuario" : body[0].userid
          }
          res.send(response);
          console.log("El usuario: " + body[0].userid + " , ya no está logado.");
          console.log("FIN PROCESO DE LOGOUT *************************************");
        }
      );
    }
  }
 );
}

module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
