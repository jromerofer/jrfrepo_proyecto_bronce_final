//ALTA DE USUARIOS

const crypt = require('../crypt');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujrf6ed/collections/";
const mLabAPIKey = "apiKey=cKW1_6BGSGLHFUmz0If5HlyuBKRT6F32";
const requestJson = require('request-json');

//GET USER V2 BBDD  // La función no es necesaria. Recupera todos los usuarios - Seguridad!!!
 function getUsersV2(req, res) {
   console.log("Comienzo proceso de buscar usuarios..............................");
   console.log("GET /apitechu/v2/users");

   var httpClient = requestJson.createClient(baseMLabURL);
   console.log("Buscando Clientes en BD");
   httpClient.get("user?"+ mLabAPIKey,
     function(err,resMLab, body) {
       var response = ! err ? body : {
         "msg" : "Error buscando usuarios"
       }
       res.send(response);
     }
   );
 }

//GET USERS V2 BY ID BBDD
function getUserByIdV2(req, res) {
  console.log("Comienzo proceso de buscar usuario por ID..............................");
  console.log("GET /apitechu/v2/users/:id");
  console.log("req.params.id : " + req.params.id);

  var id = req.params.id;
  var query = 'q={"userid" : ' + id + '}'; //comilla simple es un string literal. Estamos componiendo la query que le vamos a pasar
  console.log("userid: " + req.params.id);
  console.log("QUERY: " + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("REQUEST Buscando usuario por (userid) " + id + " en BBDD");
  console.log(baseMLabURL+ "user?" + query +"&"+ mLabAPIKey);

  httpClient.get("user?" + query +"&"+ mLabAPIKey,
    function(err,resMLab, body) {

      console.log("LENGTH: " + body.length);

      if (err) {
        var response={
          "msg" : "Error obteniendo usuarios"
        }
        res.status(500);
        console.log("ERROR DE ACCESO AL SERVIDOR.");

      } else {
        if (body.length > 0) {
          var response = body[0];
        } else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            console.log("USUARIO NOOOO ENCONTRADO POR ID");
            res.status(400);
        }
      }
      res.send(response);
      console.log("USUARIO ENCONTRADO");

    }
  );
}

//POST CREATE USER V2 BBDD
//USAMOS EL BODY (req.body.) COMO ENTRADA. NO PARAMS (req.params.);

function createUserV2 (req, res) {

  console.log("Comienzo proceso de alta de usuario ...............................");
  console.log("POST /apitechu/v2/users");
  console.log("first_name es: " + req.body.first_name);
  console.log("last_name es: " + req.body.last_name);
  console.log("email es: " + req.body.email);
  console.log("password sin encriptar: " + req.body.password);

  function randomFromInterval(min,max) {
    return Math.floor(Math.random() * (max-min+1)+min);
  }

  var userId = randomFromInterval(1000, 9999);
  console.log("userid es: " + userId);

  var newUser = {
    "userid" : userId,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  };

  console.log("Encriptado de password realizado................................");
  console.log (newUser)

  var httpClient = requestJson.createClient(baseMLabURL);

  console.log("Escribiendo usuario en BBDD.....................................");

  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body) {

      if (err) {
        console.log("Error en el proceso de alta de usuario");
        res.send({"msg" : "Error creando usuario"})
      } else {
        console.log("Alta de usuario realizada en BBDD");
        res.send({"msg" : "Usuario creado correctamente"})
        }
    }
  );
}


module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
