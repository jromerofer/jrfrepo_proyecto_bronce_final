//APITechU JRF
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require ('body-parser'); // libreria para utilizar body en la API

app.use(bodyParser.json());

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountsController = require('./controllers/AccountsController');
const tiempoAPIController = require('./controllers/TiempoAPIController');
//const MovementsController = require('./controllers/MovementsController'); Si hay tiempo,
//refactorizar. CAMBIAR TAMBIEN LA RUTA

app.listen(port); // Levantamos el servidor para escuchar en un puerto (3000 en este caso)
console.log("API escuchando en el puerto: " + port + " LOCAL VM");

/**
Los app.xxx() tienen que estar ordenados de más genérico a más específico
para que el registro de rutas se haga bien.
**/

//USERS
app.get('/apitechu/v2/users',userController.getUsersV2); //No se usa pero por si lo necesito
app.get('/apitechu/v2/users/:id',userController.getUserByIdV2);
app.post('/apitechu/v2/users',userController.createUserV2);
//LOGIN
app.post('/apitechu/v2/login',authController.loginV2);
app.post('/apitechu/v2/logout/:id',authController.logoutV2);

//ACCOUNTS
app.get('/apitechu/v2/users/:id/accounts',accountsController.getAccountsV2);  //No se usa pero por si lo necesito
app.get('/apitechu/v2/users/:id/accounts/:accountid',accountsController.getAccountsByIdV2);
app.post('/apitechu/v2/users/:id/accounts',accountsController.createAccountV2);
//MOVEMENTS
app.get('/apitechu/v2/users/:id/accounts/:accountid/movements',accountsController.getMovementsV2);
app.post('/apitechu/v2/users/:id/accounts/:accountid/movements',accountsController.createMovementV2);

//API DEL TIEMPO
app.get('/apitechu/v2/weather',tiempoAPIController.getAemetWeather);


// ***************************************************//
// ***************************************************//

//GET HELLO registro de ruta de API
app.get('/apitechu/v1/hello',
  function(req, res) {
    console.log("GET /apitechu/v1/hello");
    res.send({"msg": "Hola desde APITechU !! (:-P)  "});
  }
);
