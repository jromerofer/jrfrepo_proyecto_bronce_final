// ************************************************ //
// FUNCION DE SEGURIDAD - BCRYPT                    //
// ************************************************ //

const bcrypt = require('bcrypt');

function hash (data) {
  console.log("Hashing Data");
  return bcrypt.hashSync(data,10);
}

function checkPassword (sentPassword, userHashedPassword) {
  console.log("Checking passwords");
  return bcrypt.compareSync(sentPassword, userHashedPassword);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
