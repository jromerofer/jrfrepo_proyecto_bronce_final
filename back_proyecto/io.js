// ************************************************ //
// FUNCION DE ESCRITURA                             //
// ************************************************ //

const fs = require('fs');

function writeUserDataToFile(data) {

  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./datos_mocks/users_pwds.json", jsonUserData, "utf8",
    function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log("Datos escritos en el fichero.")
      }
    }
  );
}

module.exports.writeUserDataToFile = writeUserDataToFile
